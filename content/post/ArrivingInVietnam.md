+++
author = "Jannis Rauschke"
title = "Arriving in Vietnam | Ho-Chi-Minh-City"
description = ""
tags = [
"Saigon / Ho-Chi-Minh-City",
]
date = "2019-06-16"
categories = [
"Travel",
"Vietnam",
]
image = 'SaigonBirdview.jpg'
+++

Vietnam is a beautiful country that is currently gaining more and more popularity with young travellers. As part of my dual studies in Germany, I had the opportunity to choose a country in which I would like to work for a period of three months. Since I had never been to Asia before, I chose Vietnam to find out what all the hustle and bustle was about.

## Traffic

Ho-Chi-Minh City, often referred to as Saigon, is the largest city in southern Vietnam with more than 10 million inhabitants. As someone from a western country who had never been to an Asian country before, the first thing I noticed was the crazy traffic. I had never experienced such madness before. It's like any rush hour, but with the big difference that every remaining square inch is filled with scooters - even the pedestrian walkway. However, you will get used to it very quickly. After only 2 days I rented myself a scooter and started to participate.

{{< figure src="/images/SaigonTraffic.jpg" >}}

## Food

Food places can be found around every other corner. Often there are multiple food places right next to each other and they never seem to lose the business. The food often consists of a kind of main course with rice or noodles, a soup and some fruits. Usually there is also a kind of bitter iced tea. The Vietnamese like it spicy, but it is completely optional as each sauce is served in an extra bowl. After leaving the tourist areas, a meal only costs about 35k Dong (~1.50$). 

{{< figure src="/images/Food.jpg" >}}

## Transport

For transport the GRAB App is the right way. The functional principle is quite simple: The app recognizes your current location and you choose your destination. Then you can choose the means of transport, either car or scooter. Scooter is super cheap with a price range from 20k to 80k dong. A great tip for saving money is to simply walk 5 minutes in any direction and then check again, as prices rise drastically with the rising demand for hotspots.

## Must See

Here's a list of places I can really recommend.

### Museum of War

A place that everyone who is interested in better understanding the Vietnamese must visit. It does an excellent job of showing the course of the Vietnam War and explaining its causes. In combination with the Netflix series "The Vietnam War: A Film by Ken Burns and Lynn Novick" there is a comprehensive overview of the horrific war history of Vietnam.

### Vinhomes Central Park | Landmark 81

The wealthy part of Saigon where all the skyscrapers are located. It has a very different feeling than any other part of the city. Landmark 81 is the largest tower of the city with a big shopping center in the lower levels. It even has an ice rink in the basement! For 800k Dong (~$30) you can visit the top floor and enjoy the view over the whole city. This is also where picture at the top of this article was taken.

### Street Markets

By that I don't mean the crowded tourist trapper market Ben Thanh. I intend the markets you will encounter if you go from the city centre in all directions and bypass the main roads. These markets are absolutely overwhelming. The vendors there offer a wide range of products such as live chicken, fresh meat, vegetables, spices and more.

### Biker Shield - Bar

A small motorbike themed bar. In the evening they play some live music. Check their [website](http://bikershield.com/) for more information.

{{< figure src="/images/BMWBike.jpg" >}}

### PC Bangs

For any gamers I recommend visiting a PC bang just once. It is completly different than sitting at home in front of your highly specialized setup. The one I used to visit was Cybercore Gaming Tân Phú, which however is fairly far from the city centre.

{{< figure src="/images/PCBang.jpg" >}}

### AEON MALL Tan Phu Celadon

A crazy big mall, which has about anything you can imagine. Also a lot of fairly cheap clothing.

## VLOG

While I work during the week I take the chance to travel around Vietnam and neighbouring countries almost every weekend. Those daytrips are not only a lot of fun, but also quite interesting. So I figured I would make a not so short video covering each trip.

{{< youtube vBR64QiKHcg >}}

I hope this was helpful!