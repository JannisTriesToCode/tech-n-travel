+++
title = "About The Author"
date = "2014-04-09"
image = 'surf.jpg'
+++

Hey there! I am Jannis Rauschke, 20 years old and from Germany. Currently I am studying Computer Science with Bosch. Other than that I enjoy any kind of sports, playing video games and programming. 

<div class="timeline">
    <div class="timeline-block timeline-block-right">
        <div class="marker"></div>
        <div class="timeline-content">
            <h3>2017</h3>
            <span>Computer Science Studies</span>
            <p>
            I applied at Robert Bosch GmbH in 2016 for the dual study program in computer science, where I got accepted and started my studies in fall 2017. Since then I am alternating between working at Bosch and studying at the <a href="https://www.dhbw-stuttgart.de/themen/studienangebot/fakultaet-technik/informatik/profil/" target="_blank">DHBW Stuttgart</a>
            </p>
        </div>
    </div>
    <div class="timeline-block timeline-block-left">
        <div class="marker"></div>
        <div class="timeline-content">
            <h3>2019</h3>
            <span>YouTube</span>
            <p>
            I have always been a big fan of sharing my knowlegde and making money with it. While the latter has yet to be achieved, I had already started making short gaming related tutorials back in 2014. During my 3 months long internship in Vietnam I started taking things more seriously, thus starting a vlog series about my weekend trips in Vietnam. Anyone interested can check out my 
            <a href="https://www.youtube.com/c/jannisrauschke" target="_blank">channel</a>
            </p>
        </div>
    </div>
</div>


